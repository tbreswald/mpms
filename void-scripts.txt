# Specify XDG paths
export REPOPATH="$HOME/.local/src/void-packages"

# Default apps
export WHEEL="doas"

# Alias
alias syssv="$WHEEL sv status /var/service/*"
alias xcleanup="$WHEEL xbps-remove -foO;$WHEEL vkpurge rm all"
alias xnonfreepkgs="grep -rl '^restricted=' ${REPOPATH}/srcpkgs"

## Plugins ##
## Void packages setup
	xfi() {
		pkg="$(xbps-query -Rs "*" | awk '{ print $2 }'| fzf --multi --layout=reverse --header='Select a package to install:' --preview "xbps-query -RS {}" --preview-window=:75%:wrap:hidden --bind=space:toggle-preview)"
		if [ -n "$pkg" ]; then
	        $WHEEL xbps-install -S "$pkg"
		else
			&>/dev/null
		fi
}

	xfr() {
		pkg="$(xbps-query -m | fzf --multi --layout=reverse --header='Select a package to remove:' --preview "xbps-query -RS {}" --preview-window=:75%:wrap:hidden --bind=space:toggle-preview)"
		if [ -n "$pkg" ]; then
	        $WHEEL xbps-remove -R "$pkg"
		else
			&>/dev/null
		fi
}

# Void packages from source
# Build and install a package from void source repo
function xsbi {
        if test -d "${REPOPATH}"; then
	pkg="$(ls ${REPOPATH}/srcpkgs | fzf --multi --layout=reverse --header='Select a package to build and install:' --preview "${REPOPATH}/xbps-src show {}" --preview-window=:75%:wrap:hidden --bind=space:toggle-preview)"
	if [ -n "$pkg" ]; then
               ${REPOPATH}/xbps-src pkg -E "$pkg"
        BINPATH="/hostdir/binpkgs"
        NONFREE="/nonfree"
        if ! $WHEEL xbps-install --repository ${REPOPATH}${BINPATH} "$pkg" ; then
                if ! $WHEEL xbps-install --repository ${REPOPATH}${BINPATH}${NONFREE} "$pkg" ; then
                        return 1
                fi
        fi
	else
		&>/dev/null
	fi  
        else
        echo "void-packages not available repo bootstrapping repo"
        mkdir -p $HOME/.local/src
        git clone --depth=1 https://github.com/void-linux/void-packages.git ${REPOPATH}
        ${REPOPATH}/xbps-src binary-bootstrap
        echo XBPS_ALLOW_RESTRICTED=yes >> ${REPOPATH}/etc/conf
        echo "void-packages repo bootstrapped, try again xsbi"
        fi
}

# Pull src packages and update
function xsu {
        if test -d "${REPOPATH}"; then
                git -C ${REPOPATH} pull
                ${REPOPATH}/xbps-src update-sys
        else
                echo "Could not find void package repo."
        fi
}

# Runit
# Enable service
function runit-enable {
        service="$(ls /etc/sv | fzf --layout=reverse --header='Select a service to enable and start now:')"
	if [ -n "$service" ]; then
	$WHEEL ln -s /etc/sv/"$service" /var/service/"$service"
        echo "$service enabled" 
        else
          &>/dev/null
        fi 
}

# Disable service
function runit-disable {
        service="$(ls /var/service | fzf --layout=reverse --header='Select a service to disable and disable now:')"
	if [ -n "$service" ]; then
	$WHEEL rm -rf /var/service/"$service"
        echo "$service disabled" 
        else
          &>/dev/null
        fi
}

# groups
# add user in a group
function  addgroup {
        groupselect="$(getent group | awk -F: '{ print $1}' | fzf --layout=reverse --header='Select a group to add current user in:' )"
	if [ -n "$groupselect" ]; then
	$WHEEL usermod -a -G "$groupselect" $(whoami)
	echo "user added $groupselect group"
	else
		&>/dev/null
	fi
}
# delete user from a group
function delgroup {
        groupselect="$(printf '%s\n' $(groups $(whoami) | awk -F ':' '{ print $2 }') | fzf --layout=reverse --header='Select a group to remove current user from:' )"
	if [ -n "$groupselect" ]; then
	$WHEEL gpasswd -d $(whoami) "$groupselect"
	echo "user removed $groupselect group"
	else
		&>/dev/null
	fi
}
