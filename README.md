# mpms

## mpms - Multi Package Manager Shortcuts

at the moment it supports
- apt [+nala / + cinnamon-spice-updater] (Debian based distros / cinnamon-spice-updater is for Linux Mint)
- pacman [+yay/paru] (Arch based distributions)
- xbps-install [+void-packages] (Void Linux)
- eopkg [+eopkg3p] (Solus)
- dnf (Fedora)
- cards (NuTyX)
(- apk (alpine) needs bash and a manual install to work properly)
- zypper (openSUSE)

it supersedes JUST the four most common used commands needed for everyday usage, which are
- searching one package, 
- installing package(s) and 
- deleting package(s) including all dependencies
and also 
- updating the system


## Installation (except for alpine...without bash it's tricky...)

just clone the repo to a folder of your liking,

    git clone https://gitlab.com/tbreswald/mpms.git
       
the install.sh will add a symlink to your ~/.local/bin and add the folder .local/bin to your PATH. just 

	cd mpms
	./install.sh
    

## Usage

    USAGE:  $NAME [OPTIONS] PACKAGENAME
            $NAME [OPTIONS]
            $NAME


 OPTIONS:

     h,-h,--help	Display this message

     v,-v,--version	Display script version

     s,-s,--search	Search one package

     i,-i,--install	Installs package(s)

     w,-w,--wipe	Purges package(s), usually with dependencies

     u,-u,--update	Update repo cache und upgrade the system

     m,-m,--maint	Maintenance > updates mpms itself via gitlab

  Without any options --update is executed.

  There is a config file in $HOME/.config/mpms/ named mpms.conf which is a bash script and sourced if it's there. 
  For now the only option is to set if the script shall check for flatpak updates or not.
  Edit: I added an option for autoyes. 

  
Have Fun!

## a special thanks goes to Jake@Linux from youtube, from whose script for the void-packages I have shamelessly stolen my function to handle these
