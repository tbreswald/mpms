#! /usr/bin/env bash
[ ! -d "$HOME/.local/bin" ] && mkdir -p "$HOME/.local/bin"
[ ! -d "$HOME/.config/mpms" ] && mkdir -p "$HOME/.config/mpms" && echo -e "# flatpak_check [true,false]\nflatpak_check=false\n# autoyes[true,false]\nautoyes=false\n#git dir autoupdate [true,false]\ngu_check=false" > "$HOME/.config/mpms/mpms.conf"
[ -f "mpms" ] && ln -s "$(pwd)"/mpms "$HOME"/.local/bin/
if [[ ":$PATH:" == *":$HOME/.local/bin:"* ]]; then
	echo "path already set correctly"
else
  echo "adding \"\$HOME/.local/bin\" to your \$PATH in .bashrc"
  echo 'export PATH="$PATH:$HOME"/.local/bin' >> "$HOME/.bashrc"
fi
